package com.sudoku;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.EMPTY_SET;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;

public class Sudoku {
    private static final List<String> LOCS = asList("↖", "↑", "↗", "←", "·",
            "→", "↙",
            "↓", "↘");
    private static boolean PRINT = false;
    Integer[][] arr = new Integer[9][9];
    Set<Integer>[][] helper = new Set[9][9];
    private int computeCount;

    {
        range(0, 9).forEach(x -> range(0, 9).forEach(y -> helper[x][y] =
                new HashSet<>()));
    }

    static Sudoku read(String path) throws IOException {
        RandomAccessFile r = new RandomAccessFile(path, "r");
        String ln;
        int count = 0;
        final Pattern numReg = Pattern.compile("class=['\"]wo.+?>(\\w?)" +
                "\\s*</div>");
        Sudoku arr = new Sudoku();
        StringBuilder buff = new StringBuilder();
        while ((ln = r.readLine()) != null) buff.append(ln);
        final Matcher matcher = numReg.matcher(buff);
        while (matcher.find()) {
            if (count < 81) {
                final String group = matcher.group(1);
                if (group != null && !group.trim().isEmpty()) {
                    arr.add(count, Integer.valueOf(group));
                }
            }
            // System.out.println(count+", "+matcher.group());
            count++;
        }
        return arr;
    }

    public void add(int seq, Integer val) {
        final int r = getR(seq);
        final int c = getC(seq);

        // System.out.print(seq);
        // System.out.print(" | ");
        // System.out.print(r);
        // System.out.print(" | ");
        // System.out.print(c);
        // System.out.print(" | ");
        // System.out.println(val);

        arr[r][c] = val;
    }

    int getC(int seq) {
        return (seq / 9 //获取列当前九宫格的所处位置
                % 3 * 3 //每个九宫格的宽度是3个格子
                + seq % 3 //获取列在三列中的第几列
        )/* % 9*/;//3排九宫格只有9行取模9
    /*  0 1 2
        3 4 5
        6 7 8    */
    }

    int getR(int seq) {
        return seq / 27 //获取处于三个九宫格为单位的起始数
                * 3 //每个九宫格的高度是3个格子
                + seq / 3 //获取行处于九宫格的第几行
                % 3; //每个九宫格只有三行,取模3
    }

    int getSeq(int r, int c) {
        return r * 3 + r / 3 * 18 + c / 3 * 9 + c % 3;
    }

    public Set<Integer> getRemaining(int r, int c) {
        if (arr[r][c] != null) return EMPTY_SET;
        //region 必须验证,否则可能出现错误数字
        final Set<Integer> remaining = getFirstRemaining(r, c);
        final HashSet orginR = new HashSet(remaining);
        if (remaining.isEmpty())
            throw new RuntimeException("算法还未完备,请继续努力↖(^ω^)↗  已经发生错误运算 =>" + r + "," + c + "\n" + this);
        print("R", r, c, remaining);
        if (check(remaining)) return checkRemaining(remaining, orginR);
        //endregion
        if (check(filterNeighbor(r, c, remaining)))
            return checkRemaining(remaining, orginR);
        if (check(compareNeighbor(r, c, remaining)))
            return checkRemaining(remaining, orginR);
        print("Filter Neighbor => " + remaining);
        // System.out.println(r + ", " + c + ", " + remaining);
        return checkRemaining(remaining, orginR);
    }

    private Set<Integer> getFirstRemaining(int r, int c) {
        final Set<Integer> remaining = getAreaRemaining(r, c);
        final Set<Integer> horizontalRemaining = getHorizontalRemaining(r);
        remaining.retainAll(horizontalRemaining);
        final Set<Integer> verticalRemaining = getVerticalRemaining(c);
        remaining.retainAll(verticalRemaining);
        return remaining;
    }

    private Set<Integer> checkRemaining(Set<Integer> remaining,
                                        Set<Integer> orginR) {
        if (remaining.isEmpty() || !orginR.containsAll(remaining))
            return orginR;
        return remaining;
    }

    private Set<Integer> byHelper(int r, int c, Set<Integer> remaining) {
        Set<Integer> refer = new HashSet();
        final int seq = getSeq(r, c);
        getStreamWithoutSelf(seq).forEach(x -> refer.addAll(helper[getR(x)][getC(x)]));
        final Set<Integer> current = new HashSet(remaining);
        current.removeAll(refer);
        if (check(current)) {
            print("Helper => ", r, c, current, refer,
                    LOCS.get(seq / 9), "\n" + LOCS.get(seq % 9), remaining);
            getStreamWithoutSelf(seq).forEach(x -> {
                if (!helper[getR(x)][getC(x)].isEmpty())
                    printNotLn(LOCS.get(x % 9) + helper[getR(x)][getC(x)]);
                helper[getR(x)][getC(x)].removeAll(current);
            });
            printNotLn("\n");
            remaining.retainAll(current);
            return remaining;
        }
        return remaining;
    }

    private IntStream getStreamWithoutSelf(int seq) {
        final int si = seq / 9 * 9;
        return range(si, si + 9).filter(x -> x != seq);
    }

    public Set<Integer> compareNeighbor(int r, int c, Set<Integer> remaining) {
        if (check(compareNeighborHorizontal(r, c, remaining))) return remaining;
        check(compareNeighborVertical(r, c, remaining));
        print("CoN", r, c, remaining);
        return remaining;
    }

    public Set<Integer> compareNeighborHorizontal(int r, int c,
                                                  Set<Integer> remaining) {
        final List<Integer> rows =
                range(0, 3).filter(x -> r % 3 != x).mapToObj(x -> r / 3 * 3 + x).collect(toList());
        final List<Integer> cols =
                range(0, 3).filter(x -> c / 3 != x).mapToObj(x -> x * 3).collect(toList());

        final int horizontalCount = getHorizontalCount(rows.get(0),
                cols.get(0));
        final int horizontalCount2 = getHorizontalCount(rows.get(1),
                cols.get(1));

        final int horizontalCount3 = getHorizontalCount(rows.get(0),
                cols.get(1));
        final int horizontalCount4 = getHorizontalCount(rows.get(1),
                cols.get(0));

        final Set<Integer> hArea = getHorizontalArea(rows.get(0), cols.get(0));
        final Set<Integer> hArea2 = getHorizontalArea(rows.get(1), cols.get(1));

        final Set<Integer> hArea3 = getHorizontalArea(rows.get(0), cols.get(1));
        final Set<Integer> hArea4 = getHorizontalArea(rows.get(1), cols.get(0));
        //region First Condition

        if (horizontalCount == 3) {
            removeImpossible(remaining, hArea, hArea2);
        }
        if (horizontalCount2 == 3) {
            removeImpossible(remaining, hArea2, hArea);
        }
        //endregion
        if (check(remaining)) return remaining;

        //region Second Condition
        if (horizontalCount3 == 3) {
            removeImpossible(remaining, hArea3, hArea4);
        }
        if (horizontalCount4 == 3) {
            removeImpossible(remaining, hArea4, hArea3);
        }
        //endregion

        return remaining;
    }

    private void removeImpossible(Set<Integer> remaining, Set<Integer> hArea,
                                  Set<Integer> hArea2) {
        Set<Integer> areaCopy = new HashSet(hArea);
        areaCopy.removeAll(remaining);
        Set<Integer> area2Copy = new HashSet(hArea2);
        area2Copy.removeAll(remaining);

        area2Copy.removeAll(areaCopy);
        remaining.removeAll(area2Copy);
    }

    public Set<Integer> getHorizontalArea(int r, int c) {
        return range(0, 3).mapToObj(x -> arr[r][c / 3 * 3 + x]).filter(Objects::nonNull).collect(toSet());
    }

    public Set<Integer> compareNeighborVertical(int r, int c,
                                                Set<Integer> remaining) {
        final List<Integer> rows =
                range(0, 3).filter(x -> r / 3 != x).mapToObj(x -> x * 3).collect(toList());
        final List<Integer> cols =
                range(0, 3).filter(x -> c % 3 != x).mapToObj(x -> c / 3 * 3 + x).collect(toList());

        final int vCount = getVerticalCount(rows.get(0), cols.get(0));
        final int vCount2 = getVerticalCount(rows.get(1), cols.get(1));

        final int vCount3 = getVerticalCount(rows.get(0), cols.get(1));
        final int vCount4 = getVerticalCount(rows.get(1), cols.get(0));

        final Set<Integer> vArea = getVerticalArea(rows.get(0), cols.get(0));
        final Set<Integer> vArea2 = getVerticalArea(rows.get(1), cols.get(1));

        final Set<Integer> vArea3 = getVerticalArea(rows.get(0), cols.get(1));
        final Set<Integer> vArea4 = getVerticalArea(rows.get(1), cols.get(0));

        //region First Condition
        if (vCount == 3) {
            removeImpossible(remaining, vArea, vArea2);
        }
        if (vCount2 == 3) {
            removeImpossible(remaining, vArea2, vArea);
        }
        //endregion
        if (check(remaining)) return remaining;

        //region Second Condition
        if (vCount3 == 3) {
            removeImpossible(remaining, vArea3, vArea4);
        }
        if (vCount4 == 3) {
            removeImpossible(remaining, vArea4, vArea3);
        }
        //endregion

        return remaining;
    }

    public Set<Integer> getVerticalArea(int r, int c) {
        return range(0, 3).mapToObj(x -> arr[r / 3 * 3 + x][c]).filter(Objects::nonNull).collect(toSet());
    }

    public boolean check(Set<Integer> remaining) {
        return remaining.size() == 1;
    }

    public Set<Integer> filterNeighbor(int row, int col,
                                       Set<Integer> remaining) {
        print(row, col, remaining, "=================");
        final Set<Integer> h = range(0, 3).filter(x -> x != row % 3)
                .mapToObj(x -> getHorizontal(row / 3 * 3 + x, col))
                .reduce((x, i) -> x.retainAll(i) ? x : x).get();
        print('h', h);
        if (check(h) && checkRetainAll(remaining, h)) {
            final int horizontalCount = getHorizontalCount(row, col);
            if (horizontalCount == 2) {
                remaining.retainAll(h);
                return remaining;
            }

        }
        Set<Integer> v = range(0, 3).filter(x -> x != col % 3)
                .mapToObj(x -> getVertical(col / 3 * 3 + x, row))
                .reduce((x, i) -> x.retainAll(i) ? x : x).get();
        print('v', v);
        if (check(v) && checkRetainAll(remaining, v)) {
            final int verticalCount = getVerticalCount(row, col);
            if (verticalCount == 2) {
                remaining.retainAll(v);
                return remaining;
            }

        }
        Set<Integer> merge = h;
        merge.retainAll(v);
        print("Merge", merge);
        if (checkRetainAll(remaining, merge)) {
            remaining.retainAll(merge);
            return remaining;
        }
        // r.addAll(c);
        // if (r.size()>1) set.retainAll(r);
        print(remaining, "=============");
        return remaining;
    }

    public boolean checkRetainAll(Set<Integer> remaining, Set<Integer> merge) {
        if (merge.isEmpty()) return false;
        if (remaining.containsAll(merge)) return true;
        AtomicBoolean check = new AtomicBoolean(false);
        merge.forEach(x -> {
            if (remaining.contains(x)) check.set(true);
        });
        return check.get();
    }

    private Set<Integer> getArea(int row, int col) {
        return getArea(getSeq(row, col));
    }

    static void print(Object... o) {
        if (!PRINT) return;
        stream(o).forEach(obj -> System.out.print(obj + " "));
        if (o.length > 0) System.out.println();
    }

    public Set<Integer> getAreaRemaining(int row, int col) {
        return getAreaRemaining(getSeq(row, col));
    }

    public Stream<Integer> getAll() {
        return range(1, 10).boxed();
    }

    public Set<Integer> getAreaRemaining(int seq) {
        final Set<Integer> area = getArea(seq);
        return getAll().filter(x -> !area.contains(x)).collect(toSet());
    }

    public Set<Integer> getArea(int seq) {
        if (seq < 0 || seq > 80) return null;
        final int srt = seq / 9 * 9;
        return range(srt, srt + 9).mapToObj(i -> arr[getR(i)][getC(i)]).filter(Objects::nonNull).collect(toSet());
    }

    public Set<Integer> getHorizontal(int row) {
        return stream(arr[row], 0, 9).filter(Objects::nonNull).collect(toSet());
    }

    public Set<Integer> getVertical(int col) {
        return range(0, 9).mapToObj(x -> arr[x][col]).filter(Objects::nonNull).collect(toSet());
    }

    private Set<Integer> getHorizontal(int row, int col) {
        return range(0, 9).filter(x -> x / 3 != col / 3).mapToObj(x -> arr[row][x]).filter(Objects::nonNull).collect(toSet());
    }

    private Set<Integer> getVertical(int col, int row) {
        return range(0, 9).filter(x -> x / 3 != row / 3).mapToObj(x -> arr[x][col]).filter(Objects::nonNull).collect(toSet());
    }

    public Set<Integer> getHorizontalRemaining(int row) {
        final Set<Integer> horizontal = getHorizontal(row);
        return getAll().filter(x -> !horizontal.contains(x)).collect(Collectors.toSet());
    }

    int getHorizontalCount(int row, int col) {
        return range(0, 3).mapToObj(i -> (arr[row][col / 3 * 3 + i])).filter(Objects::nonNull).mapToInt(x -> 1).sum();
    }

    int getVerticalCount(int row, int col) {
        return range(0, 3).mapToObj(i -> (arr[row / 3 * 3 + i][col])).filter(Objects::nonNull).mapToInt(x -> 1).sum();
    }

    public Set<Integer> getVerticalRemaining(int col) {
        final Set<Integer> vertical = getVertical(col);
        return getAll().filter(x -> !vertical.contains(x)).collect(toSet());
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        final List<Integer> sep = asList(2, 5);
        range(0, 9).forEach(x -> {
            s.append("|");
            range(0, 9).forEach(i -> s.append(arr[x][i] == null ? " " :
                    arr[x][i])
                    .append(sep.contains(i) ? "|  " : "").append("|"));
            s.append('\n');
            if (sep.contains(x)) s.append("\n");
        });
        // for (int i = 0; i < arr.length; i++) {
        //     for (int j = 0; j < arr[i].length; j++) {
        //         if (j > 0) s.append(",");
        //         s.append(getSeq(i, j));
        //     }
        //     s.append('\n');
        // }
        return s.toString();
    }

    public Sudoku getAnswer() {
        final List<Integer> remaining =
                range(0, 81).filter(x -> getSeqInt(x) == null).boxed().collect(toList());
        // System.out.println(remaining);
        AtomicInteger count = new AtomicInteger();
        // final ExecutorService es = Executors.newFixedThreadPool(10);
        long st = System.currentTimeMillis();

        //region 执行解谜程序
        Map<Integer, Set<Integer>> supposeMap = new HashMap<>();
        while (!remaining.isEmpty()) {
            List<Integer> remove = new ArrayList<>();
            remaining.forEach(seq -> /*es.submit(()->*/{
                final int r = getR(seq);
                final int c = getC(seq);
                final Set<Integer> re = getRemaining(r, c);
                helper[r][c].clear();
                count.getAndIncrement();
                final int size = re.size();
                if (size == 1) {
                    // final Set<Integer> areaRemaining = getAreaRemaining(r,
                    // c);
                    // areaRemaining.retainAll(getHorizontalRemaining(r));
                    // areaRemaining.retainAll(getVerticalRemaining(c));
                    // if (!areaRemaining.containsAll(re)) {
                    //     System.err.println(r+","+c+" is ERROR => " +re);
                    //     return;
                    // }
                    re.forEach(x -> arr[r][c] = x);
                    getStreamWithoutSelf(seq).forEach(x -> helper[getR(x)][getC(x)].removeAll(re));
                    remove.add(seq);
                } else if (size == 0 && arr[r][c] == null) {
                    System.err.println(r + "," + c);
                } else {
                    helper[r][c].addAll(re);
                }
            })/*)*/;
            //region Helper处理
            remaining.forEach(seq -> {
                final int r = getR(seq);
                final int c = getC(seq);
                if (helper[r][c].isEmpty()) return;
                Set<Integer> re = new HashSet(helper[r][c]);
                if (check(byHelper(r, c, re)))
                    re = checkRemaining(re, helper[r][c]);
                count.getAndIncrement();
                Set<Integer> helperWork = null;
                if (re.size() == 1) {
                    helperWork = new HashSet(re);
                }
                re = checkRemaining(re, getFirstRemaining(r, c));
                final int size = re.size();

                if (size == 1) {
                    re.forEach(x -> arr[r][c] = x);
                    helper[r][c].clear();
                    Set<Integer> finalRe = re;
                    getStreamWithoutSelf(seq).forEach(x -> helper[getR(x)][getC(x)].removeAll(finalRe));
                    remove.add(seq);
                } else if (size == 0 && arr[r][c] == null) {
                    print("Helper Warning ", r, c, "出现异常," +
                            "请检查程序逻辑问题!!!");
                } else if (helperWork != null) {
                    print("Helper Check fail", LOCS.get(seq / 9),
                            LOCS.get(seq % 9), re, helperWork);
                } else {
                    Set<Set<Integer>> filterTwoHelper = new HashSet();
                    Set<Set<Integer>> filterTwo = new HashSet();
                    Set<Set<Integer>> filterThree = new HashSet();
                    Set<Integer> filterTwoSeq = new HashSet();
                    Set<Set<Integer>> removeTwo = new HashSet();
                    getStreamWithoutSelf(seq).forEach(x -> {
                        final Set<Integer> set = helper[getR(x)][getC(x)];
                        if (set.size() == 2) {
                            if (filterTwoHelper.contains(set)) {
                                filterTwo.add(set);
                                filterTwoSeq.add(x);
                                supposeMap.put(x, set);
                            } else filterTwoHelper.add(set);
                        } else removeTwo.add(set);
                        if (set.size() == 3) {
                            if (filterThree.contains(set)) {
                                supposeMap.put(x, set);
                            }
                            filterThree.add(set);
                        }
                    });
                    if (!filterTwo.isEmpty()) {
                        removeTwo.forEach(x -> filterTwo.forEach(x::removeAll));
                        final Set<Integer>[] filterTwoArr =
                                (Set<Integer>[]) filterTwo.toArray(new Set[0]);
                        final Integer[] filterTwoSeqArr =
                                filterTwoSeq.toArray(new Integer[0]);
                        for (int i = 0; i < filterTwo.size(); i++) {
                            Set<Integer> re2 = filterTwoArr[i];
                            if (re2.size() == 1) {
                                final Integer seq1 = filterTwoSeqArr[i];
                                re2 = checkRemaining(re2,
                                        getFirstRemaining(getR(seq1),
                                                getC(seq1)));
                                print("Helper 2 => ", LOCS.get(seq1 / 9),
                                        LOCS.get(seq1 % 9),
                                        re2);
                                if (re2.size() > 1) continue;
                                supposeMap.remove(seq1);
                                re2.forEach(x -> arr[getR(seq1)][getC(seq1)]
                                        = x);
                                re2.clear();
                                remove.add(seq1);
                            }
                        }
                    }
                }
            });
            //endregion
            if (!remaining.removeAll(remove)) break;
        }
        if (getSudokuCount() != 0) {
            final Sudoku clone = clone();
            final ThreadLocal<AtomicInteger> local =
                    new ThreadLocal<>();
            if (local.get() == null) local.set(new AtomicInteger());
            supposeMap.forEach((x, set) -> {
                if (getSudokuCount() == 0) return;
                new HashSet<>(set).forEach(v -> {
                    try {
                        supposeNumber(x, v);
                    } catch (Exception e) {
                        print("SupposeNumber Warning : " +
                                local.get().addAndGet(1) + " => " + e);
                    }
                    if (getSudokuCount() == 0) return;
                    clone.copyArrTo(this);
                });
            });
        }
        //endregion
        computeCount = count.get();
        print("Compute => " + count + ", " + (System.currentTimeMillis() - st));
        return this;
    }

    private void supposeNumber(Integer seq, Integer v) {
        setSeqInt(seq, v);
        getAnswer();
    }

    public Integer getInt(String group) {
        if (group == null || group.trim().isEmpty()) return null;
        return Integer.parseInt(group);
    }

    public Integer getSeqInt(int seq) {
        return arr[getR(seq)][getC(seq)];
    }

    public void setSeqInt(int seq, Integer v) {
        final Integer unitInt = arr[getR(seq)][getC(seq)];
        if (unitInt != null)
            throw new RuntimeException("此位置[" + LOCS.get(seq / 9)
                    + LOCS.get(seq % 9) + "]已经存在值" + unitInt);
        arr[getR(seq)][getC(seq)] = v;
    }

    public Sudoku clone() {
        Sudoku sdk = new Sudoku();
        copyArrTo(sdk);
        return sdk;
    }

    void copyArrTo(Sudoku sdk) {
        range(0, 9).forEach(i -> range(0, 9).forEach(x -> {
            sdk.arr[i][x] = this.arr[i][x];
        }));
    }

    public static void printNotLn(Object s) {
        if (PRINT) System.out.print(s);
    }

    void printHelper() {
        print("##############  " + LOCS.get(0));
        range(0, 81).forEach(i -> {
            final int r = getR(i);
            final int c = getC(i);
            Set<Integer> o = helper[r][c];
            if (i % 9 == 3) print();
            if (i % 9 == 6) print();
            if (i > 0 && i % 9 == 0)
                print("\n##############  " + LOCS.get(i / 9));
            if (o.isEmpty()) {
                return;
            }
            printNotLn(LOCS.get(i % 9) + " => " + o + "| ");
        });
    }

    public long getSudokuCount() {
        return stream(arr, 0, 9).filter(x -> stream(x, 0, 9).filter(y -> y == null).count() != 0).count();
    }

    public void printComputeCount() {
        System.out.println("Compute Count => " + computeCount);
    }
}
