package com.sudoku;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ALL")
public class Main {

	// private static final String path = "C:\\Users\\Administrator\\
	// .IntelliJIdea2019" +
	//         ".3\\config\\scratches\\scratch_5.html";
	private static final String path =
			new File("src/java/com/sudoku/sudoku.html").getAbsolutePath();

	public static void main(String[] args) throws IOException {
		final Sudoku sdk = Sudoku.read(path);
		System.out.println(sdk);
		// System.out.println(sdk);
		// assert sdk.get(0, 2) == 1;
		// assert sdk.get(1, 2) == 9;
		// assert sdk.get(2, 0) == 8;
		// assert sdk.get(7, 8) == 5;
		// assert sdk.get(8, 6) == null;
		// assert sdk.get(8, 8) == 3;
		// System.out.println(sdk);
		//
		// System.out.println(sdk.getArea(0,7));
		// System.out.println(sdk.getAreaRemaining(0,7));
		// System.out.println(sdk.getHorizontal(0));
		// System.out.println(sdk.getHorizontalRemainint(0));
		// System.out.println(sdk.getVertical(0));
		// System.out.println(sdk.getVerticalRemaining(0));
		// System.out.println(sdk.getRemaining(0,0));
		// System.out.println(sdk.getRemaining(0,2));
		System.out.println(sdk.getAnswer());
	}
}
