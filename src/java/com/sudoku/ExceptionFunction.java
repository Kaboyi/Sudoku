package com.sudoku;

@FunctionalInterface
public interface ExceptionFunction<T, R> {
    // public ExceptionFunction() {
    //     super("算法还未完备,请继续努力↖(^ω^)↗  ");
    // }
    // public ExceptionFunction(String msg) {
    //     super("算法还未完备,请继续努力↖(^ω^)↗  "+msg);
    // }

    R apply(T t) throws Exception;
}
