package com.sudoku;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Tools {
    static Field getField(Class clazz, String fieldName) {
        try {
            return clazz.getField(fieldName);
        } catch (NoSuchFieldException e) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException ignored) {
            }
		}
		return null;
	}

	static Object getFieldValue(Field field, Object o) {
		try {
			return field.get(o);
		} catch (IllegalAccessException e) {
			field.setAccessible(true);
			try {
				return field.get(o);
			} catch (IllegalAccessException ignored) {
			}
		}
		return null;
	}

	static void setFieldValue(Field field, Object o, Object v) {
		try {
			field.setAccessible(true);
			field.set(o, v);
		} catch (IllegalAccessException e) {
			// try {
			// 	//region FINAL修饰符取消,静态常量基本属性无法修改
            // 	System.out.println("~FINAL");
            // 	Field modifiers = field.getClass().getDeclaredField
            // 	("modifiers");
            // 	modifiers.setAccessible(true);
            // 	modifiers.setInt(field, field.getModifiers() & ~Modifier
            // 	.FINAL);
            // 	//endregion
            // 	field.set(o, v);
            // } catch (IllegalAccessException | NoSuchFieldException ignored) {
            // }
        }
    }

    protected static Field setPrintTrue(Sudoku sdk) throws IllegalAccessException {
        final Field printF = getField(Sudoku.class, "PRINT");
        assert printF != null;
        setFieldValue(printF, sdk, true);
        assertEquals(true, printF.get(sdk));
        return printF;
    }
}
