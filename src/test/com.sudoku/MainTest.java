package com.sudoku;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import static com.sudoku.Sudoku.print;
import static com.sudoku.Tools.getField;
import static com.sudoku.Tools.setFieldValue;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.IntStream.range;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {

    @Test
    void main() throws IOException, IllegalAccessException,
            InstantiationException {
        final Field pathF = getField(Main.class, "path");
        final Main main = Main.class.newInstance();
        final String path = (String) Tools.getFieldValue(pathF, main);
        final Sudoku sdk = Sudoku.read(path);

        //region 打开PRINT标记
        final Field printF = Tools.setPrintTrue(sdk);
        //endregion
        sdk.getAnswer();
        print(sdk);
        sdk.printHelper();

        // /*Main.main(new String[0]);*/
        setFieldValue(printF, sdk, false);
        assertEquals(false, printF.get(sdk));
    }

    @Test
    public void retain() {
        final HashSet hashSet = new HashSet(asList(1, 2));
        hashSet.retainAll(new HashSet(singletonList(3)));
        print(hashSet);
    }

    // @Test
    public void testPrint() throws InterruptedException, IOException {
        final String s = "VDKFD\nsdflf";
        System.out.print(s);
        range(0, s.length()).forEach(x -> System.out.print("\b"));
        TimeUnit.SECONDS.sleep(1L);

        String os = System.getProperty("os.name");
        System.out.print(os);
        // if(os.contains("Windows")) Runtime.getRuntime().exec("cls");
        // else Runtime.getRuntime().exec("clear");
    }
}