package com.sudoku;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.sudoku.Sudoku.print;
import static com.sudoku.Sudoku.printNotLn;
import static com.sudoku.Tools.setFieldValue;
import static com.sudoku.Tools.setPrintTrue;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.IntStream.range;
import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
class SudokuTest {

	private static String DATA_HTML;
	private static Sudoku init_sdk;
	private Sudoku sdk;

	@BeforeAll
	static void beforeTest() throws IOException {
		final File af = new File(requireNonNull(Sudoku.class.getClassLoader().getResource(".")).getFile());
		final File parentFile = af.getParentFile().getParentFile();
		DATA_HTML = parentFile.getPath()+"\\v0.0.1\\sudoku.html";
		init_sdk = Sudoku.read(DATA_HTML);
	}

	@BeforeEach
	void setUp() {
		sdk= init_sdk.clone();
	}

	@Test
	void read() throws IOException {
		final Pattern numReg = Pattern.compile("class=['\"]wo.+?>(\\w?)\\s*</div>");
		final RandomAccessFile file = new RandomAccessFile(DATA_HTML, "r");
		byte[] bytes = new byte[(int) file.length()];
		file.readFully(bytes);
		String text = new String(bytes, UTF_8);
		assertTrue(text.length() > 0);
		text = text.replace("\r\n", "");
		// Sudoku.print(text);
		final Matcher matcher = numReg.matcher(text);
		int count = 0;
		while (matcher.find()) {
			final String group = matcher.group(1);
			assertEquals(sdk.getInt(group), sdk.arr[sdk.getR(count)][sdk.getC(count)]);
			count++;
		}
		assertEquals(81, count);
	}

	@Test
	void add() {
		int except = 19999;
		int seq = 0;
		sdk.add(seq, except);
		assertEquals(sdk.getSeqInt(seq), except);
		seq = 5;
		except = 13923;
		sdk.add(seq, except);
		assertEquals(except, sdk.getSeqInt(seq));
	}

	@Test
	void getRemaining() {
		assertEquals(new HashSet(asList(3, 4, 5, 8)), sdk.getRemaining(0, 2));
	}

	@Test
	void filterNeighbor() {
		final Set<Integer> remaining = sdk.getRemaining(1, 1);
		sdk.filterNeighbor(1, 1, remaining);
		// assertEquals(remaining, new HashSet(asList(3, 4, 5, 8)));
		try {
			System.out.println(remaining);
			assertEquals(9, remaining.toArray()[0]);
			assertEquals(1, remaining.size());
		} catch (Exception e) {
			print(sdk);
		}
	}

	@Test
	void testPrint() {
		print("S");
	}

	@Test
	void getAreaRemaining() {
		final Set<Integer> areaRemaining = sdk.getAreaRemaining(2);
		assertTrue(areaRemaining.retainAll(sdk.getRemaining(0, 2)));
		assertEquals(areaRemaining.size(), 4);
		assertEquals(new HashSet(asList(3, 4, 5, 8)), areaRemaining);
	}

	@Test
	void getAll() {
		assertTrue(sdk.getAll().allMatch(x -> x < 10 && x > 0));
		assertEquals(sdk.getAll().count(), 9);
		final Set<Integer> collect = range(1, 10).boxed().collect(Collectors.toSet());
		assertEquals(collect, sdk.getAll().collect(Collectors.toSet()));
	}

	@Test
	void testGetAreaRemaining() {
		assertEquals(sdk.getAreaRemaining(2), sdk.getAreaRemaining(0, 2));
	}

	@Test
	void getArea() {
		assertEquals(new HashSet(asList(1, 2, 6)), sdk.getArea(2));
	}

	@Test
	void getHorizontal() {
		assertEquals(new HashSet(asList(1, 6, 7, 9)), sdk.getHorizontal(0));
	}

	@Test
	void getVertical() {
		assertEquals(new HashSet(asList(7, 9)), sdk.getVertical(2));
	}

	@Test
	void getHorizontalRemaining() {
		assertEquals(new HashSet(asList(2, 3, 4, 5, 8)), sdk.getHorizontalRemaining(0));
	}

	@Test
	void getHorizontalCount() {
		assertEquals(2, sdk.getHorizontalCount(0, 2));
	}

	@Test
	void getVerticalCount() {
		assertEquals(0, sdk.getVerticalCount(0, 2));
		assertEquals(2, sdk.getVerticalCount(2, 0));
	}

	@Test
	void getVerticalRemaining() {
		assertEquals(new HashSet(asList(1, 2, 3, 4, 5, 6, 8)), sdk.getVerticalRemaining(2));
	}

	@Test
	void testToString() {
		assertEquals("|6|1| |  |9| | |  | |7| |\n" +
				"|2| | |  | |3| |  | | |8|\n" +
				"| | | |  |1| | |  |9|5| |\n" +
				"\n" +
				"|9|3| |  | |2|8|  | | | |\n" +
				"| | | |  |7| |1|  | | | |\n" +
				"| | |7|  | | | |  |2| |6|\n" +
				"\n" +
				"|3| |9|  | |1| |  | | | |\n" +
				"| | | |  | |4| |  | |6| |\n" +
				"| |2| |  | | | |  | |9|5|\n", sdk.toString());
	}

	@Test
	void getAnswer() throws IllegalAccessException {
		sdk.getAnswer();
		final Field f = setPrintTrue(sdk);
		printNotLn(sdk);
		sdk.printHelper();
		setFieldValue(f, sdk, false);
		assertEquals(0,
				sdk.getSudokuCount());

	}

	@Test
	void getAnswer2() {
		range(0, 81).forEach(x -> sdk.add(x, (x + 1) % 10));
		assertEquals(0,
				sdk.getSudokuCount());
		assertEquals(0,
				stream(sdk.arr, 9, 9).filter(x -> stream(x, 0, 9).filter(y -> y == null).count() == 0).count());

	}

	@Test
	void getC() {
		assertEquals(0, sdk.getC(0));
		assertEquals(0, sdk.getC(27));
		assertEquals(8, sdk.getC(26));
		assertEquals(8, sdk.getC(80));
	}

	@Test
	void getR() {
		assertEquals(0, sdk.getR(0));
		assertEquals(3, sdk.getR(27));
		assertEquals(2, sdk.getR(26));
		assertEquals(8, sdk.getR(80));
	}

	@Test
	void compareNeighbor() {
		final Set<Integer> remaining = sdk.getRemaining(0, 2);
		sdk.add(12, 4);
		sdk.add(14, 7);

		sdk.add(sdk.getSeq(2, 4), 4);
		sdk.add(sdk.getSeq(2, 5), 1);
		sdk.add(sdk.getSeq(0, 8), 8);
		sdk.compareNeighbor(0, 2, remaining);
		assertEquals(new HashSet(asList(3, 4, 5, 8)), remaining);
	}

	@Test
	void compareNeighborHorizontal() {
		final int seq = sdk.getSeq(1, 3);
		final Set<Integer> remaining = sdk.getRemaining(1, 3);
		sdk.add(26, 3);
		sdk.compareNeighborHorizontal(1, 3, remaining);
		assertEquals(new HashSet(asList(4, 5, 6)), remaining);
	}

	@Test
	void getHorizontalArea() {
		final Set<Integer> horizontalArea = sdk.getHorizontalArea(0, 0);
		print(sdk);
		assertEquals(2, horizontalArea.size());
		assertEquals(new HashSet(asList(1, 6)), horizontalArea);
	}

	@Test
	void compareNeighborVertical() {
		final int seq = sdk.getSeq(1, 3);
		final Set<Integer> remaining = sdk.getRemaining(1, 3);
		sdk.add(sdk.getSeq(5, 5), 5);
		sdk.compareNeighborVertical(1, 3, remaining);
		assertEquals(new HashSet(asList(4, 5, 6)), remaining);
	}

	@Test
	void getVerticalArea() {
		final Set<Integer> verticalArea = sdk.getVerticalArea(0, 0);
		print(sdk);
		assertEquals(2, verticalArea.size());
		assertEquals(new HashSet(asList(2, 6)), verticalArea);
	}

	@Test
	void check() {
	}

	@Test
	void checkRetainAll() {
		final HashSet remaining = new HashSet(asList(1, 3, 5));
		final HashSet merge = new HashSet(asList(3, 5));
		final HashSet merge2 = new HashSet(asList(3));
		final HashSet merge3 = new HashSet(asList(6));
		assertTrue(sdk.checkRetainAll(remaining, merge));
		assertTrue(sdk.checkRetainAll(remaining, merge2));
		assertFalse(sdk.checkRetainAll(remaining, merge3));
	}

	@Test
	void getInt() {
	}

	@Test
	void getSeqInt() {
	}

	@Test
	void testClone() {
	}
}