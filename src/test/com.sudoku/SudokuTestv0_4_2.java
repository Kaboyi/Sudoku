package com.sudoku;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import static com.sudoku.Sudoku.printNotLn;
import static com.sudoku.Tools.setFieldValue;
import static com.sudoku.Tools.setPrintTrue;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("ALL")
class SudokuTestv0_4_2 {
    @BeforeAll
    static void beforeTest() throws IOException {
        final File af =
                new File(requireNonNull(Sudoku.class.getClassLoader().getResource(".")).getFile());
        final File parentFile = af.getParentFile().getParentFile();
        DATA_HTML = parentFile.getPath() + "\\v0.4\\sudoku2.html";
        init_sdk = Sudoku.read(DATA_HTML);
    }

    @Test
    void getAnswer() throws IllegalAccessException {
        sdk.getAnswer();
        final Field f = setPrintTrue(sdk);
        printNotLn(sdk);
        // sdk.printHelper();
        sdk.printComputeCount();
        setFieldValue(f, sdk, false);
        assertEquals(0,
                stream(sdk.arr, 0, 9).filter(x -> stream(x, 0, 9).filter(y -> y == null).count() != 0).count());

    }

    @Test
    void read() {
        System.out.println(sdk);
    }


    private static String DATA_HTML;

    private static Sudoku init_sdk;

    private Sudoku sdk;

    @BeforeEach
    void setUp() {
        sdk = init_sdk.clone();
    }

}