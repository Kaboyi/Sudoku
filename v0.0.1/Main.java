import com.sudoku.Sudoku;

import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		Sudoku sudoku = new Sudoku(null);
		assert sudoku.getR(0)==0;
		assert sudoku.getC(0)==0;
		assert sudoku.getR(20)==0;
		assert sudoku.getC(20)==8;
		assert sudoku.getR(6)==2;
		assert sudoku.getC(6)==0;
		assert sudoku.getR(26)==2;
		assert sudoku.getC(26)==8;
		assert sudoku.getR(27)==3;
		assert sudoku.getC(27)==0;
		assert sudoku.getR(80)==8;
		assert sudoku.getC(80)==8;

		// Sudoku s = Sudoku.getFromFile();
		// if (s==null) s=Sudoku.getFromURL();
		// s.getOtherHorizontal(2).forEach(System.out::println);
		// s.getOtherHorizontal(3).forEach(System.out::println);
		// s.getOtherHorizontal(7).forEach(System.out::println);
	}
}
