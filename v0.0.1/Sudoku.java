import sun.misc.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

public class Sudoku {
	Integer[][] arr = new Integer[9][9];
	Pattern pattern = Pattern.compile("class=\"wo.*?(\\s|\\w)</div>");

	public Sudoku(String s) {
		if (s==null||s.isEmpty()) return;
		Matcher matcher = pattern.matcher(s);
		int seq=0;
		while (matcher.find()) {
			String group = matcher.group(1);
			if (!group.trim().isEmpty()) {
				setSeq(seq,Integer.valueOf(group));
			}
			seq++;
		}
		// System.out.println(s);
	}

	private void setSeq(int seq, Integer i) {
		arr[getR(seq)][getC(seq)]=i;
	}

	public int getR(int seq) {
		return seq/9*3%6+seq%9/3;
		//00 08<=>0 20
		//20 28<=>6 26
		//30 88<=>27 80
	}

	public int getC(int seq) {
		return 0;
	}

	public static Sudoku getFromURL() throws IOException {
		URL url = new URL("https://www.sudoku-cn.com");
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(30*1000);
		conn.setReadTimeout(30*1000);
		InputStream is = conn.getInputStream();
		byte[] bytes = IOUtils.readFully(is, conn.getContentLength(), true);
		String s = new String(bytes, UTF_8);
		s = s.replace("\n", "");;
		return new Sudoku(s);
	}

	public static Sudoku getFromFile() {
		try {
			RandomAccessFile f = new RandomAccessFile("test/sudoku.html", "r");
			StringBuilder buff = new StringBuilder();
			String ln;
			while ((ln=f.readLine())!=null) buff.append(buff);
			return new Sudoku(buff.toString());
		} catch (IOException e) {
			return null;
		}
	}

	public Set<Integer> getOtherHorizontal(int row) {
		return stream(arr[row], 0, 9).filter(Objects::nonNull).collect(toSet());
	}
}
